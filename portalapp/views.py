from django.shortcuts import render, get_object_or_404
from .models import PortalApp

# Create your views here.
def korporat(request):
    apps = PortalApp.objects.filter(category_id=1)
    return render(request, 'portalapp/unit.html', {'apps':apps})

def pusdiklat(request):
    apps = PortalApp.objects.filter(category_id=2)
    return render(request, 'portalapp/unit.html', {'apps':apps})

def pusertif(request):
    apps = PortalApp.objects.filter(category_id=3)
    return render(request, 'portalapp/unit.html', {'apps':apps})

def puslitbang(request):
    apps = PortalApp.objects.filter(category_id=4)
    return render(request, 'portalapp/unit.html', {'apps':apps})
