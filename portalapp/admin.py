from django.contrib import admin
from .models import PortalApp
from .models import Kategori

class PortalAppAdmin(admin.ModelAdmin):
    readonly_fields = ('datecreated',)
    list_display = ('title', 'link', 'description', 'category',)

class KategoriAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(PortalApp, PortalAppAdmin)
admin.site.register(Kategori, KategoriAdmin)
