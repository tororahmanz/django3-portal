from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class PortalApp(models.Model):
    title = models.CharField(max_length=50)
    link = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    category = models.ForeignKey('Kategori', on_delete=models.CASCADE, null=False)
    datecreated = models.DateTimeField(auto_now_add=True)
    createdby = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class Kategori(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=20)

    def __int__(self):
        return self.id
    def __str__(self):
        return self.category
