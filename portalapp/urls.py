from django.urls import path
from . import views
from django.contrib import admin

app_name = 'portalapp'

urlpatterns = [
    path('korporat/', views.korporat, name='korporat'),
    path('pusdiklat/', views.korporat, name='pusdiklat'),
    path('pusertif/', views.korporat, name='pusertif'),
    path('', views.korporat, name='korporat'),

]
